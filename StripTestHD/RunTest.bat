set RECSTRIP=RecStrip3_Win32.exe

del /S /Q new\*
mkdir new\RS_sep new\RS_sepstrip new\RS_gem new\RS_gemstrip new\RS_append new\RS_merge new\RS_mergestrip new\RS_mergestripped

%RECSTRIP% -c "Sportschau.rec" new/RS_sep > new/RS_sep/RS.log
%RECSTRIP% -c -s -e "Sportschau.rec" new/RS_sepstrip > new/RS_sepstrip/RS.log

ren "Sportschau.cut" "Sportschau_sep.cut"
ren "Sportschau_gem.cut" "Sportschau.cut"
%RECSTRIP% -r "Sportschau.rec" "new/RS_gem/Sportschau_cut.rec" > new/RS_gem/RS.log
%RECSTRIP% -r -s -e "Sportschau.rec" "new/RS_gemstrip/Sportschau_cutstrip.rec" > new/RS_gemstrip/RS.log
ren "Sportschau.cut" "Sportschau_gem.cut"
ren "Sportschau_sep.cut" "Sportschau.cut"

%RECSTRIP% -s -e "new/RS_gem/Sportschau_cut.rec" "new/RS_gem/Sportschau_cutstrip.rec" > new/RS_gem/RS2.log

%RECSTRIP% -s -e "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-1)_strip.rec" > new/RS_sep/RS2a.log
%RECSTRIP% -s -e "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-2)_strip.rec" > new/RS_sep/RS2b.log
%RECSTRIP% -s -e "new/RS_sep/Sportschau (Cut-3).rec" "new/RS_sep/Sportschau (Cut-3)_strip.rec" > new/RS_sep/RS2c.log


ren "Sportschau.cut" "Sportschau_sep.cut"
ren "Sportschau_app.cut" "Sportschau.cut"
copy "Sportschau.rec.*" new\RS_append
copy "Sportschau.cut" new\RS_append
%RECSTRIP% -a -r "new/RS_append/Sportschau.rec" "Sportschau.rec" > new/RS_append/RS.log
ren "Sportschau.cut" "Sportschau_app.cut"
ren "Sportschau_sep.cut" "Sportschau.cut"

%RECSTRIP% -m "new/RS_merge/Sportschau_merge.rec" "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-3).rec" > new/RS_merge/RS.log
%RECSTRIP% -s -e "new/RS_merge/Sportschau_merge.rec" "new/RS_merge/Sportschau_mergestrip.rec" > new/RS_merge/RS2.log

%RECSTRIP% -m -s -e "new/RS_mergestrip/Sportschau_mergestrip.rec" "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-3).rec" > new/RS_mergestrip/RS.log

%RECSTRIP% -m "new/RS_mergestripped/Sportschau_mergestripped.rec" "new/RS_sepstrip/Sportschau (Cut-1).rec" "new/RS_sepstrip/Sportschau (Cut-2).rec" "new/RS_sepstrip/Sportschau (Cut-3).rec" > new/RS_mergestripped/RS.log
