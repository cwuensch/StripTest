#!/bin/bash
echo 'Run a standardized RecStrip test for HD video'
echo '(c) 2021 Christian Wuensch'
echo

if [ "$1" == "-h" ] ; then
  echo "Usage:  $0 <version> [<RecStrip_binary>]"
  echo '   or:  RS_VER=3 RunTest.sh'
  echo
  echo '<RecStrip_binary> defaults to "RecStrip" resp. "RecStrip3"'
  echo
  exit 0
fi

cd "$(dirname "${BASH_SOURCE[0]}")"

# Check variables and parameters
if [ -n "$1" ] ; then RS_VER=$1 ; fi
if [ -n "$RS_VER" ] && [ "$RS_VER" -le "2" ] ; then RS_VER= ; fi
if [ -n "$2" ] ; then RECSTRIP=$2 ; fi
if [ -z "$RECSTRIP" ] ; then RECSTRIP=./bin/RecStrip${RS_VER} ; fi
REFDIR=ref${RS_VER}

if [ ! -f $RECSTRIP ] ; then
  echo "RecStrip binary '$RECSTRIP' not found!"
  exit 1000
fi
if [ -z "$REFDIR" ] || [ ! -d $REFDIR ] ; then
  echo "No reference folder '$REFDIR'!"
  exit 1001
fi
echo "Using RecStrip $RS_VER at '$RECSTRIP' and reference '$REFDIR'."
echo

# Prepare output folder
rm -rf new
mkdir -p new/RS_sep new/RS_sepstrip new/RS_gem new/RS_gemstrip new/RS_append new/RS_merge new/RS_mergestrip new/RS_mergestripped


# 1. RecStrip: Copy into SEPARATE files (without and with stipping)
echo "1. RecStrip: Copy into SEPARATE files (without and with stipping)"
$RECSTRIP -c "Sportschau.rec" new/RS_sep > new/RS_sep/RS.log
$RECSTRIP -c -s -e "Sportschau.rec" new/RS_sepstrip > new/RS_sepstrip/RS.log

# 2. RecStrip: Remove scenes to create a COMMON file (without and with stripping)
echo "2. RecStrip: Remove scenes to create a COMMON file (without and with stripping)"
mv "Sportschau.cut" "Sportschau_sep.cut"; mv "Sportschau_gem.cut" "Sportschau.cut"
$RECSTRIP -r "Sportschau.rec" "new/RS_gem/Sportschau_cut.rec" > new/RS_gem/RS.log
$RECSTRIP -r -s -e "Sportschau.rec" "new/RS_gemstrip/Sportschau_cutstrip.rec" > new/RS_gemstrip/RS.log
mv "Sportschau.cut" "Sportschau_gem.cut"; mv "Sportschau_sep.cut" "Sportschau.cut"

# 3. RecStrip: STRIP the non-stripped files from steps 1 and 2
echo "3. RecStrip: STRIP the non-stripped files from steps 1 and 2"
$RECSTRIP -s -e "new/RS_gem/Sportschau_cut.rec" "new/RS_gem/Sportschau_cutstrip.rec" > new/RS_gem/RS2.log

$RECSTRIP -s -e "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-1)_strip.rec" > new/RS_sep/RS2a.log
$RECSTRIP -s -e "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-2)_strip.rec" > new/RS_sep/RS2b.log
$RECSTRIP -s -e "new/RS_sep/Sportschau (Cut-3).rec" "new/RS_sep/Sportschau (Cut-3)_strip.rec" > new/RS_sep/RS2c.log


# 4. RecStrip: APPEND 2 scenes of original video onto itself (without stripping)
echo "4. RecStrip: APPEND 2 scenes of original video onto itself (without stripping)"
mv "Sportschau.cut" "Sportschau_sep.cut" ; mv "Sportschau_app.cut" "Sportschau.cut"
cp "Sportschau.rec"* "Sportschau.cut" new/RS_append/
$RECSTRIP -a -r "new/RS_append/Sportschau.rec" "Sportschau.rec" > new/RS_append/RS.log
mv "Sportschau.cut" "Sportschau_app.cut" ; mv "Sportschau_sep.cut" "Sportschau.cut"


# 5. RecStrip: MERGE the 3 scenes from step 1 into new file (and strip it afterwards)
echo "5. RecStrip: MERGE the 3 scenes from step 1 into new file (and strip it afterwards)"
$RECSTRIP -m "new/RS_merge/Sportschau_merge.rec" "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-3).rec" > new/RS_merge/RS.log
$RECSTRIP -s -e "new/RS_merge/Sportschau_merge.rec" "new/RS_merge/Sportschau_mergestrip.rec" > new/RS_merge/RS2.log

# 6. RecStrip: MERGE and STRIP the the scenes from step 1 within a single step
echo "6. RecStrip: MERGE and STRIP the the scenes from step 1 within a single step"
$RECSTRIP -m -s -e "new/RS_mergestrip/Sportschau_mergestrip.rec" "new/RS_sep/Sportschau (Cut-1).rec" "new/RS_sep/Sportschau (Cut-2).rec" "new/RS_sep/Sportschau (Cut-3).rec" > new/RS_mergestrip/RS.log

# 7. RecStrip: MERGE the STRIPPED versions of the scenes from step 1
echo "7. RecStrip: MERGE the STRIPPED versions of the scenes from step 1"
$RECSTRIP -m "new/RS_mergestripped/Sportschau_mergestripped.rec" "new/RS_sepstrip/Sportschau (Cut-1).rec" "new/RS_sepstrip/Sportschau (Cut-2).rec" "new/RS_sepstrip/Sportschau (Cut-3).rec" > new/RS_mergestripped/RS.log


# Check for differences
echo
echo "Check for differences"
diff -r --brief --exclude=*.log new $REFDIR
RETURN=$?
if [ "$RETURN" -eq "0" ] ; then
  echo "All files identical!"
fi
exit $RETURN
