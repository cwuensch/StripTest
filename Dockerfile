FROM ubuntu:latest

ARG SRCDIR=StripTest

#Install x86 support (optional)
RUN dpkg --add-architecture i386
RUN apt-get update && apt-get -y install libc6:i386 libncurses5:i386 libstdc++6:i386

RUN ["/bin/sh", "-c", "mkdir -p /StripTest"]
WORKDIR /StripTest

ADD ${SRCDIR}.tar.gz /StripTest/
COPY ${SRCDIR}/RunTest.sh /StripTest/RunTest.sh

VOLUME StripTest/bin

ENTRYPOINT [ "./RunTest.sh" ]
#CMD ["2 ./bin/RecStrip"]
