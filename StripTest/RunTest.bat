set RECSTRIP=RecStrip_Win32.exe

del /S /Q new\*
mkdir new\RS_sep new\RS_sepstrip new\RS_gem new\RS_gemstrip

%RECSTRIP% -c "Wintersport.rec" new/RS_sep > new/RS_sep/RS.log
%RECSTRIP% -c -s -e "Wintersport.rec" new/RS_sepstrip > new/RS_sepstrip/RS.log

ren "Wintersport.cut" "Wintersport_sep.cut"
ren "Wintersport_gem.cut" "Wintersport.cut"
%RECSTRIP% -r "Wintersport.rec" "new/RS_gem/Wintersport_cut.rec" > new/RS_gem/RS.log
%RECSTRIP% -r -s -e "Wintersport.rec" "new/RS_gemstrip/Wintersport_cutstrip.rec" > new/RS_gemstrip/RS.log
ren "Wintersport.cut" "Wintersport_gem.cut"
ren "Wintersport_sep.cut" "Wintersport.cut"

%RECSTRIP% -s -e "new/RS_gem/Wintersport_cut.rec" "new/RS_gem/Wintersport_cutstrip.rec" > new/RS_gem/RS2.log

%RECSTRIP% -s -e "new/RS_sep/Wintersport (Cut-1).rec" "new/RS_sep/Wintersport (Cut-1)_strip.rec" > new/RS_sep/RS2a.log
%RECSTRIP% -s -e "new/RS_sep/Wintersport (Cut-2).rec" "new/RS_sep/Wintersport (Cut-2)_strip.rec" > new/RS_sep/RS2b.log
%RECSTRIP% -s -e "new/RS_sep/Wintersport (Cut-3).rec" "new/RS_sep/Wintersport (Cut-3)_strip.rec" > new/RS_sep/RS2c.log
